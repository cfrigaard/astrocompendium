function x = Bl(l,T)
%B_lamda(T)

c=299792458;
k=1.380658E-23;
h=6.6260755E-34;

if l==0  
  x=0;
  return 
end

x = 2*h*c*c/l^5/(exp(h*c/(l*k*T))-1);
