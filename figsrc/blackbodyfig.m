clear

l=2.997924E-7;
%v=1E15;

T1=6000;
T2=7500;
T3=9000;
T4=2.725; 

minx=0;
maxx=2000E-9;
maxy=20E14;
reso=100;

x1=bb(T1,minx,maxx,reso,1);
x2=bb(T2,minx,maxx,reso,1);
x3=bb(T3,minx,maxx,reso,1);

y1=bb(T1,minx,maxy,reso,2);
y2=bb(T2,minx,maxy,reso,2);
y3=bb(T3,minx,maxy,reso,2);

x4=bb(T4,0,50E-4,reso,1);
y4=bb(T4,0,60E10,reso,2);

figure(1)
clc
subplot(2,1,1)
hold off
plot(x1(:,1)*1E9,x1(:,2),'k-')
hold on
plot(x2(:,1)*1E9,x2(:,2),'k-')
plot(x3(:,1)*1E9,x3(:,2),'k-')

z=483;
f=bl(z*1E-9,T1)
plot(z,f,'xk')
plot(z,f,'ok')
z=386;
f=bl(z*1E-9,T2)
plot(z,f,'xk')
plot(z,f,'ok')
z=322;
f=bl(z*1E-9,T3)
plot(z,f,'xk')
plot(z,f,'ok')

%title('Blackbody for T=6000, 7500 and 9000 K')

grid
xlabel('Wavelenght \lambda in nm');
ylabel('B_\lambda(T) in W m^{-2} m^{-1} sterad^{-1}');

text(0.35*1000 ,0.45E14,'B_\lambda(T=6000 K,\lambda=483 nm)=0.32E14')
text(0.25*1000,1.1E14  ,'B_\lambda(T=7500 K,\lambda=386 nm)=0.97E14')
text(0.4 *1000,2.3E14  ,'B_\lambda(T=9000 K,\lambda=322 nm)=2.42E14')

subplot(2,1,2)
hold off
plot(y1(:,1)/1E9,y1(:,2),'k-')
hold on
plot(y2(:,1)/1E9,y2(:,2),'k-')
plot(y3(:,1)/1E9,y3(:,2),'k-')

z=353E3;
f=bv(z*1E9,T1)
plot(z,f,'xk')
plot(z,f,'ok')
z=441E3;
f=bv(z*1E9,T2)
plot(z,f,'xk')
plot(z,f,'ok')
z=529E3;
f=bv(z*1E9,T3)
plot(z,f,'xk')
h=plot(z,f,'ok')

%yyax(h,[1 10]);

grid
xlabel('Frequency \nu in GHz');
ylabel('B_{\nu}(T) in W m^{-2} Hz^{-1} sterad^{-1}');

text(0.3E6,0.5E-7 ,'B_\nu(T=6000 K,\nu=353E3 GHz) = 0.41E-7')
text(0.35E6,0.9E-7,'B_\nu(T=7500 K,\nu=441E3 GHz) = 0.80E-7')
text(0.4E6,1.2E-7 ,'B_\nu(T=9000 K,\nu=529E3 GHz) = 1.38E-7')

print -deps2 fig_blackbody

figure(2)
clc

subplot(2,1,1)
hold off
plot(x4(:,1)*1000,x4(:,2),'k-')

hold on
z=1.07;
plot(z,bl(z*1E-3,T4),'xk')
plot(z,bl(z*1E-3,T4),'ok')

text(1.0,7E-4  ,'B_\lambda(T=2.725 K,\lambda=1.07 mm) = 6.2E-4')

%title('Blackbody for T=2.725 K')

grid
xlabel('Wavelength \lambda in mm');
ylabel('B_{\lambda}(T) in W m^{-2} m^{-1} sterad^{-1}');

subplot(2,1,2)
hold off

plot(y4(:,1)/1E9,y4(:,2),'k-')

%hold on
%[ax,h1,h2] =plotyy(0,0,600,3E-18);

hold on

z=159;
f=bv(z*1E9,T4)
plot(z,f,'xk')
plot(z,f,'ok')

text(155,3.3E-18 ,'B_\nu(T=2.725 K,\nu=159 GHz) = 3.7E-18')

axis tight

grid

xlabel('Frequency \nu in GHz');
ylabel('B_\nu(T) in W m^{-2} Hz^{-1} sterad^{-1}');

print -deps2 fig_blackbody_27

load g2v.m
plot(g2v(:,1),g2v(:,2),'k-');

T=5785;
x1=bb(T,minx,1100E-9,reso,1);
T=5636;
x2=bb(T,minx,1100E-9,reso,1);

figure(3)
clc

%subplot(1,1,1)
hold off
plot(x1(:,1)*1E9,x1(:,2),'k-')
hold on
plot(x2(:,1)*1E9,x2(:,2),'k:')
plot(g2v(:,1)/10,g2v(:,2)*2.417*1E13,'k-')

legend('T=5785 K','T=5636 K')

grid
axis tight
%title('Spectrum of the sun (G2-V) and ideal blackbody for T=5785 K')
xlabel('Wavelength \lambda in nm');
ylabel('B_{\lambda}(T) in W m^{-2} m^{-1} sterad^{-1}');

text(393,0.7E13,'K');
text(410,1.0E13,'H');
text(430,1.5E13,'G');
text(580,2.0E13,'D');
text(650,1.56E13,'H_\alpha');

print -deps2 fig_blackbody_g2v
