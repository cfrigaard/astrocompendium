function x = Bb(T,minx,maxx,reso,type)
%B_lamda(T)

n=0;
y=zeros(reso+100,2);

for i=minx:(maxx-minx)/reso:maxx
 n=n+1;
 y(n,1)=i;
 if type==1
   y(n,2)=Bl(i,T);
 else
   y(n,2)=Bv(i,T);
 end
end

x=[y(1:n,1) y(1:n,2)];
