(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.0'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     23034,        551]*)
(*NotebookOutlinePosition[     23677,        573]*)
(*  CellTagsIndexPosition[     23633,        569]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^m]\ \ x^n\ \[DifferentialD]x\)\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] \
\[DifferentialD]x\>\"" \[Equal] \ \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^2] 
            x\ \ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^2 \
\[DifferentialD]x\>\"" \[Equal] \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ x^2 \[DifferentialD]x]\), "\
\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^3 \
\[DifferentialD]x\>\""\  \[Equal] \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ x^3 \[DifferentialD]x]\), "\
\[IndentingNewLine]", 
    \(Assuming[{x, a, n} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^n \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ x^
              n \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a, n, m} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 1\  && 
        m \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^m] x^n \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x^m]\ \ x^
              n \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a, n, m} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 1\  && 
        m \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x] x^n \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              Exp[\(-a\)\ x]\ \ x^n \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] \[DifferentialD]x" \
\[Equal] \@\[Pi]\/\(2\ \@a\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x \[DifferentialD]x" \
\[Equal] 1\/\(2\ a\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^2 \[DifferentialD]x" \
\[Equal] \@\[Pi]\/\(4\ a\^\(3/2\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^3 \[DifferentialD]x" \
\[Equal] 1\/\(2\ a\^2\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^2] x^n \[DifferentialD]x" \
\[Equal] 1\/2\ a\^\(\(-\(1\/2\)\)\ \((1 + n)\)\)\ Gamma[\(1 + n\)\/2]\)], \
"Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x^m] x^n \[DifferentialD]x" \
\[Equal] \(a\^\(-\(\(1 + n\)\/m\)\)\ Gamma[\(1 + n\)\/m]\)\/m\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)Exp[-a x] x^n \[DifferentialD]x" \
\[Equal] a\^\(\(-1\) - n\)\ Gamma[1 + n]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]\_\(-\[Infinity]\)\%\
\[Infinity] Exp[\(-a\)\ x^m]\ \ x^n\ \[DifferentialD]x\)\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{\(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2] \[DifferentialD]x\>\"" \[Equal] \ \ \[Integral]\_\(-\[Infinity]\)\%\
\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ \[DifferentialD]x]\), \
"\[IndentingNewLine]", \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2] x  \[DifferentialD]x \[Equal] 0\>\""]\), "\[IndentingNewLine]", \
\(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2] x^2 \[DifferentialD]x\>\"" \[Equal] \ \ \[Integral]\_\(-\[Infinity]\)\%\
\[Infinity] 
              Exp[\(-a\)\ x^2]\ x^2\ \[DifferentialD]x]\), "\
\[IndentingNewLine]", \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2] x^3 \[DifferentialD]x \[Equal] 0\>\""]\), "\[IndentingNewLine]", \
\(Assuming[{x, a, n} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2] x^n \[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_\(-\[Infinity]\)\%\
\[Infinity] Exp[\(-a\)\ x^2]\ \ x^
              n \[DifferentialD]x]\), "\[IndentingNewLine]", 
    StyleBox[\(odd\ n\),
      "Text"], "\[IndentingNewLine]", \(Assuming[{x, a, n} \[Element] 
          Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2]\nx^{(2n+1)} \[DifferentialD]x \[Equal] 0\>\""\ ]\), \
"\[IndentingNewLine]", 
    StyleBox[\(even\ n\),
      "Text"], "\[IndentingNewLine]", \(Assuming[{x, a, n} \[Element] 
          Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a \
x^2]  x^{(2n)} \[DifferentialD]x \[Equal]\>\""\  \[Equal] \ \[Integral]\_\(-\
\[Infinity]\)\%\[Infinity] 
              Exp[\(-a\)\ x^2]\ \ x^\((2  
                  n)\)\ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] \
\[DifferentialD]x" \[Equal] \@\[Pi]\/\@a\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] x  \
\[DifferentialD]x \[Equal] 0"\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] x^2 \
\[DifferentialD]x" \[Equal] \@\[Pi]\/\(2\ a\^\(3/2\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] x^3 \
\[DifferentialD]x \[Equal] 0"\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] x^n \
\[DifferentialD]x" \[Equal] 
      1\/2\ \((1 + \((\(-1\))\)\^n)\)\ a\^\(\(-\(1\/2\)\)\ \((1 + n)\)\)\ \
Gamma[\(1 + n\)\/2]\)], "Output"],

Cell[BoxData[
    \(n\ odd\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2] x^(2n+1) \
\[DifferentialD]x \[Equal] 0"\)], "Output"],

Cell[BoxData[
    \(even\ n\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)Exp[-a x^2]  x^{(2n)} \
\[DifferentialD]x \[Equal]" \[Equal] 
      1\/2\ \((1 + \((\(-1\))\)\^\(2\ n\))\)\ a\^\(\(-\(1\/2\)\) - n\)\ Gamma[
          1\/2 + n]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]\_0\%\[Infinity] x^
                n/\ \((Exp[a\ x] - 1)\)\ \[DifferentialD]x\)\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1 / (Exp[a x]-1) \[DifferentialD]x\>" == \
\[Integral]1/\ \((Exp[a\ x] - 
                  1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x / (Exp[a x]-1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              x/\ \((Exp[a\ x] - 
                  1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^2/ (Exp[a x]-1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
                x^2\ /\ \((Exp[a\ x] - 
                  1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^3/ (Exp[a x]-1) \
\[DifferentialD]x\>\""\ \  \[Equal] \ \[Integral]\_0\%\[Infinity] 
                x^3\ /\ \((Exp[a\ x] - 
                  1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a, n} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^n/ (Exp[a x]-1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] x^
                n/\ \((Exp[a\ x] - 1)\)\ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\[Integral]1 / (Exp[a x]-1) \[DifferentialD]x" \[Equal] \(-x\) + 
        Log[\(-1\) + \[ExponentialE]\^\(a\ x\)]\/a\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x / (Exp[a x]-1) \[DifferentialD]x" \
\[Equal] \[Pi]\^2\/\(6\ a\^2\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^2/ (Exp[a x]-1) \[DifferentialD]x" \
\[Equal] \(2\ Zeta[3]\)\/a\^3\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^3/ (Exp[a x]-1) \[DifferentialD]x" \
\[Equal] \[Pi]\^4\/\(15\ a\^4\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^n/ (Exp[a x]-1) \[DifferentialD]x" \
\[Equal] a\^\(\(-1\) - n\)\ Gamma[1 + n]\ PolyLog[1 + n, 1]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]\_0\%\[Infinity] x^
                n/\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x\)\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1 / (Exp[a x]+1) \[DifferentialD]x\>" == \
\[Integral]1/\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x]\), "\n", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x / (Exp[a x]+1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              x/\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x]\), "\n", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^2/ (Exp[a x]+1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
                x^2\ /\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x]\), "\n", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^3/ (Exp[a x]+1) \
\[DifferentialD]x\>\""\ \  \[Equal] \ \[Integral]\_0\%\[Infinity] 
                x^3\ /\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x]\), "\n", 
    \(Assuming[{x, a, n} \[Element] Reals\  && \ a > 0\  && \ 
        n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^n/ (Exp[a x]+1) \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] x^
                n/\ \((Exp[a\ x] + 1)\)\ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\[Integral]1 / (Exp[a x]+1) \[DifferentialD]x" \[Equal] 
      x - Log[1 + \[ExponentialE]\^\(a\ x\)]\/a\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x / (Exp[a x]+1) \[DifferentialD]x" \
\[Equal] \[Pi]\^2\/\(12\ a\^2\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^2/ (Exp[a x]+1) \[DifferentialD]x" \
\[Equal] \(3\ Zeta[3]\)\/\(2\ a\^3\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^3/ (Exp[a x]+1) \[DifferentialD]x" \
\[Equal] \(7\ \[Pi]\^4\)\/\(120\ a\^4\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^n/ (Exp[a x]+1) \[DifferentialD]x" \
\[Equal] 2\^\(-n\)\ \((\(-1\) + 2\^n)\)\ a\^\(\(-1\) - n\)\ Gamma[
          1 + n]\ Zeta[1 + n]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    RowBox[{" ", "\[IndentingNewLine]", 
      StyleBox[\(Integrals\ involving\ \ \(\[Integral]1/\((a\ Exp[c\ x] + \ 
                    b)\)\ \[DifferentialD]x\)\),
        "Section"]}]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1/(Exp[x]-1) \[DifferentialD]x\>"\  \[Equal] \ \
\[Integral]1/\((Exp[x] - 1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1/(Exp[x]+1) \[DifferentialD]x\>"\  \[Equal] \ \
\[Integral]1/\((Exp[x] + 1)\)\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1/(1-Exp[-x]) \[DifferentialD]x\>"\  \[Equal] \ \
\[Integral]1/\((1 - 
                  Exp[\(-x\)])\)\ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, "\<\[Integral]1/(1+Exp[-x]) \[DifferentialD]x\>"\  \[Equal] \ \
\[Integral]1/\((1 + 
                  Exp[\(-x\)])\)\ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a, b, c} \[Element] 
        Reals\ , "\<\[Integral]1/(a Exp[c x]+ b) \[DifferentialD]x\>"\  \
\[Equal] \ \[Integral]1/\((a\ Exp[c\ x] + \ 
                  b)\)\ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\[Integral]1/(Exp[x]-1) \[DifferentialD]x" \[Equal] \(-x\) + 
        Log[\(-1\) + \[ExponentialE]\^x]\)], "Output"],

Cell[BoxData[
    \("\[Integral]1/(Exp[x]+1) \[DifferentialD]x" \[Equal] 
      x - Log[1 + \[ExponentialE]\^x]\)], "Output"],

Cell[BoxData[
    \("\[Integral]1/(1-Exp[-x]) \[DifferentialD]x" \[Equal] 
      Log[\(-1\) + \[ExponentialE]\^x]\)], "Output"],

Cell[BoxData[
    \("\[Integral]1/(1+Exp[-x]) \[DifferentialD]x" \[Equal] 
      Log[1 + \[ExponentialE]\^x]\)], "Output"],

Cell[BoxData[
    \("\[Integral]1/(a Exp[c x]+ b) \[DifferentialD]x" \[Equal] 
      x\/b - Log[b + a\ \[ExponentialE]\^\(c\ x\)]\/\(b\ c\)\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]\_0\%\[Infinity] x^n\ Log[
              1 - \ Exp[\(-x\)]]\ \[DifferentialD]x\)\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\) Log[1- Exp[-x]] \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity]\ 
          Log[1 - \ 
                Exp[\(-x\)]]\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x  Log[1- Exp[-x]] \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
            x\ \ Log[
              1 - \ Exp[\(-x\)]]\ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^2 Log[1- Exp[-x]] \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] x^2\ Log[
              1 - \ Exp[\(-x\)]]\ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^3 Log[1- Exp[-x]] \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] x^3\ Log[
              1 - \ Exp[\(-x\)]]\ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a, n} \[Element] Reals\  && \ 
        a > 0\  && \ \ n \[GreaterEqual] 
          1, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^n Log[1- Exp[-x]] \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] x^n\ Log[
              1 - \ Exp[\(-x\)]]\ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\) Log[1- Exp[-x]] \[DifferentialD]x" \
\[Equal] \(-\(\[Pi]\^2\/6\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x  Log[1- Exp[-x]] \[DifferentialD]x" \
\[Equal] \(-Zeta[3]\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^2 Log[1- Exp[-x]] \
\[DifferentialD]x" \[Equal] \(-\(\[Pi]\^4\/45\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^3 Log[1- Exp[-x]] \
\[DifferentialD]x" \[Equal] \(-6\)\ Zeta[5]\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^n Log[1- Exp[-x]] \
\[DifferentialD]x" \[Equal] \(-n\)\ Gamma[n]\ PolyLog[2 + n, 1]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Integrals\ involving\ \ \(\[Integral]1\/Cosh^2[a\ x]\
\ \ \[DifferentialD]x\)\  = \ \[Integral]Sech^2[a\ x]\ \ \[DifferentialD]x\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)\!\(\((x/2)\)^2\/\(4\\\
\ Cosh[x/2]^2\)\) \[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\
\[Infinity]\((x/2)\)^2\/\(4\ Cosh[x/2]^2\)\ \ \ \ \[DifferentialD]x\
\[IndentingNewLine]]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)\!\(\((\
x/2)\)^2\/\(4\\\ Cosh[x/2]^2\)\) \[DifferentialD]x\>\""\  \[Equal] \ \
\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\((x/2)\)^2\/\(4\ Cosh[x/2]^2\)\
\ \ \ \ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \ \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)1 / (Cosh[a x])^2  \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
              1\ /\ \((Cosh[
                    a\ x])\)^2\ \ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)1 / \
(Cosh[a x])^2 \[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_\(-\[Infinity]\
\)\%\[Infinity] 
              1\ /\ \((Cosh[
                    a\ x])\)^2\ \[DifferentialD]x]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_0\%\[Infinity]\)x^2 / (Cosh[a x])^2 \
\[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_0\%\[Infinity] 
                x^2\ /\ \((Cosh[
                    a\ x])\)^2\ \ \[DifferentialD]x]\), \
"\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)x^2 / \
(Cosh[a x])^2 \[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_\(-\[Infinity]\
\)\%\[Infinity] x^2\ /\ \((Cosh[a\ x])\)^2\ \ \[DifferentialD]x]\)}], "Input"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)\!\(\((x/2)\)^2\/\(4\\ \
Cosh[x/2]^2\)\) \[DifferentialD]x" \[Equal] \[Pi]\^2\/24\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)\!\(\((x/2)\)^2\/\(4\\ \
Cosh[x/2]^2\)\) \[DifferentialD]x" \[Equal] \[Pi]\^2\/12\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)1 / (Cosh[a x])^2  \[DifferentialD]x" \
\[Equal] 1\/a\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)1 / (Cosh[a x])^2 \
\[DifferentialD]x" \[Equal] 2\/a\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_0\%\[Infinity]\)x^2 / (Cosh[a x])^2 \
\[DifferentialD]x" \[Equal] \[Pi]\^2\/\(12\ a\^3\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)x^2 / (Cosh[a x])^2 \
\[DifferentialD]x" \[Equal] \[Pi]\^2\/\(6\ a\^3\)\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox[\(Etc\ Integrals\),
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
    \(\(\(\[IndentingNewLine]\)\(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)\!\(Sin[\
x]^2\/x^2\) \[DifferentialD]x\>\""\  \[Equal] \ \[Integral]\_\(-\[Infinity]\)\
\%\[Infinity] Sin[x]^2\/x^2\ \[DifferentialD]x\[IndentingNewLine]]\)\)\)], \
"Input"],

Cell[BoxData[
    \("\!\(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\)\!\(Sin[x]^2\/x^2\) \
\[DifferentialD]x" \[Equal] \[Pi]\)], "Output"]
}, Open  ]],

Cell[BoxData[
    StyleBox["Differentials",
      "Section"]], "Text"],

Cell[CellGroupData[{

Cell[BoxData[{
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[PartialD]\_x\) Log[1-Exp [-x]]\>\"" \[Equal] \ \
\[PartialD]\_x\ Log[1 - Exp\ [\(-x\)]]]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[PartialD]\_x\) Log[a + b Exp [c x]]\>\"" \[Equal] \
\ \[PartialD]\_x\ Log[a\  + \ b\ Exp\ [c\ x]]]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[PartialD]\_x\) (x Log[-1+Exp [-x]])\>\"" \[Equal] \
\ \[PartialD]\_x\ \((x\ Log[\(-1\) + 
                  Exp\ [\(-x\)]])\)]\), "\[IndentingNewLine]", 
    \(Assuming[{x, a} \[Element] Reals\  && \ 
        a > 0, \*"\"\<\!\(\[PartialD]\_x\) (a x^n Log[b+c Exp [d x]])\>\"" \
\[Equal] \ \[PartialD]\_x\ \((a\ x^n\ Log[b + c\ Exp\ [d\ x]])\)]\)}], "Input"],

Cell[BoxData[
    \("\!\(\[PartialD]\_x\) Log[1-Exp [-x]]" \[Equal] \[ExponentialE]\^\(-x\)\
\/\(1 - \[ExponentialE]\^\(-x\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[PartialD]\_x\) Log[a + b Exp [c x]]" \[Equal] \(b\ c\ \
\[ExponentialE]\^\(c\ x\)\)\/\(a + b\ \[ExponentialE]\^\(c\ x\)\)\)], "Output"],

Cell[BoxData[
    \("\!\(\[PartialD]\_x\) (x Log[-1+Exp [-x]])" \[Equal] \(-\(\(\
\[ExponentialE]\^\(-x\)\ x\)\/\(\(-1\) + \[ExponentialE]\^\(-x\)\)\)\) + 
        Log[\(-1\) + \[ExponentialE]\^\(-x\)]\)], "Output"],

Cell[BoxData[
    \("\!\(\[PartialD]\_x\) (a x^n Log[b+c Exp [d x]])" \[Equal] \(a\ c\ d\ \
\[ExponentialE]\^\(d\ x\)\ x\^n\)\/\(b + c\ \[ExponentialE]\^\(d\ x\)\) + 
        a\ n\ x\^\(\(-1\) + n\)\ Log[
            b + c\ \[ExponentialE]\^\(d\ x\)]\)], "Output"]
}, Open  ]]
},
FrontEndVersion->"5.0 for Microsoft Windows",
ScreenRectangle->{{32, 800}, {0, 560}},
WindowSize->{594, 538},
WindowMargins->{{0, Automatic}, {Automatic, 0}}
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 172, 3, 61, "Text"],

Cell[CellGroupData[{
Cell[1951, 58, 2110, 39, 387, "Input"],
Cell[4064, 99, 129, 2, 51, "Output"],
Cell[4196, 103, 123, 2, 43, "Output"],
Cell[4322, 107, 140, 2, 48, "Output"],
Cell[4465, 111, 128, 2, 43, "Output"],
Cell[4596, 115, 176, 3, 43, "Output"],
Cell[4775, 120, 166, 2, 56, "Output"],
Cell[4944, 124, 143, 2, 41, "Output"]
}, Open  ]],
Cell[5102, 129, 174, 3, 60, "Text"],

Cell[CellGroupData[{
Cell[5301, 136, 2089, 37, 385, "Input"],
Cell[7393, 175, 137, 2, 51, "Output"],
Cell[7533, 179, 129, 2, 41, "Output"],
Cell[7665, 183, 155, 2, 48, "Output"],
Cell[7823, 187, 130, 2, 41, "Output"],
Cell[7956, 191, 225, 4, 43, "Output"],
Cell[8184, 197, 40, 1, 29, "Output"],
Cell[8227, 200, 135, 2, 41, "Output"],
Cell[8365, 204, 41, 1, 29, "Output"],
Cell[8409, 207, 243, 4, 43, "Output"]
}, Open  ]],
Cell[8667, 214, 176, 3, 61, "Text"],

Cell[CellGroupData[{
Cell[8868, 221, 1424, 24, 279, "Input"],
Cell[10295, 247, 151, 2, 43, "Output"],
Cell[10449, 251, 136, 2, 46, "Output"],
Cell[10588, 255, 136, 2, 42, "Output"],
Cell[10727, 259, 138, 2, 46, "Output"],
Cell[10868, 263, 166, 2, 41, "Output"]
}, Open  ]],
Cell[11049, 268, 176, 3, 61, "Text"],

Cell[CellGroupData[{
Cell[11250, 275, 1280, 20, 279, "Input"],
Cell[12533, 297, 139, 2, 43, "Output"],
Cell[12675, 301, 137, 2, 46, "Output"],
Cell[12815, 305, 143, 2, 43, "Output"],
Cell[12961, 309, 146, 2, 46, "Output"],
Cell[13110, 313, 203, 3, 41, "Output"]
}, Open  ]],
Cell[13328, 319, 211, 4, 82, "Text"],

Cell[CellGroupData[{
Cell[13564, 327, 1092, 20, 279, "Input"],
Cell[14659, 349, 137, 2, 30, "Output"],
Cell[14799, 353, 125, 2, 30, "Output"],
Cell[14927, 357, 127, 2, 30, "Output"],
Cell[15057, 361, 122, 2, 30, "Output"],
Cell[15182, 365, 153, 2, 44, "Output"]
}, Open  ]],
Cell[15350, 370, 176, 3, 61, "Text"],

Cell[CellGroupData[{
Cell[15551, 377, 1458, 26, 279, "Input"],
Cell[17012, 405, 135, 2, 45, "Output"],
Cell[17150, 409, 129, 2, 41, "Output"],
Cell[17282, 413, 139, 2, 45, "Output"],
Cell[17424, 417, 133, 2, 41, "Output"],
Cell[17560, 421, 153, 2, 41, "Output"]
}, Open  ]],
Cell[17728, 426, 187, 3, 116, "Text"],

Cell[CellGroupData[{
Cell[17940, 433, 1869, 32, 360, "Input"],
Cell[19812, 467, 149, 2, 47, "Output"],
Cell[19964, 471, 164, 2, 47, "Output"],
Cell[20131, 475, 121, 2, 42, "Output"],
Cell[20255, 479, 135, 2, 42, "Output"],
Cell[20393, 483, 140, 2, 46, "Output"],
Cell[20536, 487, 154, 2, 46, "Output"]
}, Open  ]],
Cell[20705, 492, 73, 2, 40, "Text"],

Cell[CellGroupData[{
Cell[20803, 498, 332, 5, 85, "Input"],
Cell[21138, 505, 138, 2, 42, "Output"]
}, Open  ]],
Cell[21291, 510, 70, 2, 40, "Text"],

Cell[CellGroupData[{
Cell[21386, 516, 839, 13, 170, "Input"],
Cell[22228, 531, 140, 2, 43, "Output"],
Cell[22371, 535, 162, 2, 44, "Output"],
Cell[22536, 539, 215, 3, 43, "Output"],
Cell[22754, 544, 264, 4, 46, "Output"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

