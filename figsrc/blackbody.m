clear

l=2.997924E-7;
v=1E15;
T=6000;


figure(1)
minx=0;
maxx=1000E-9;
reso=100;

x=bb(T,minx,maxx,reso,1);

hold off
plot(x(:,1),x(:,2),'.-')
hold on
title(strcat('Blackbody: B lambda(T), T=',' K' ))
xlabel('Lambda');
ylabel('B(T)');


figure(2)

minx=3.5273E14;
maxx=3.5274E14;

reso=100;
x=bb(T,minx,maxx,reso,2);

hold off
axis tight
plot(x(:,1),x(:,2),'.-')
hold on
title(strcat('Blackbody: B lambda(T), T=',' K' ))
xlabel('Lambda');
ylabel('B(T)');
grid

%axis([minx maxx 0 5E-8])

lx=[10 1000 3000 6000 10000];
ly=[5.8785E11 5.88E13 1.76375E14 3.53E14 5.8785E14];

figure(4)
plot(ly,lx,'x-')
