function x = Bv(v,T)
%B_nu(T)

c=299792458;
k=1.380658E-23;
h=6.6260755E-34;

if v==0  
  x=0;
  return 
end

z=exp(h*v/(k*T))-1;
if z==0   
  x=0;
  return 
end


x = 2*h*v^3/c/c/z;
