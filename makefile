TEXFILE=astrocompendium

doc:
	rm -f $(TEXFILE).dvi
	latex $(TEXFILE).tex <latexinput.txt

ps:
	rm -f $(TEXFILE).ps
	dvips -t a4 -f $(TEXFILE).dvi >$(TEXFILE).ps
	nice ps2pdf -r1200 $(TEXFILE).ps 
	nice gzip $(TEXFILE).ps
	mv $(TEXFILE).ps.gz $(TEXFILE).ps
	#ghostview -scalebase 2 $(TEXFILE).ps 

index:
	makeindex $(TEXFILE)
	cat $(TEXFILE).ind      | grep -v mathbf > $(TEXFILE).ind.temp
	cat $(TEXFILE).ind.temp | grep -v hbox > $(TEXFILE).ind
	rm 	$(TEXFILE).ind.temp
	#bibtex $(TEXFILE)

all: 
	make doc
	make index
	make ps

clean:
	@ rm -f *.log *.dvi *.aux *.toc *.idx *.ilg *.ind *.pdf *.ps *.bbl *.bib *.blg
	@ rm -rf .xvpics figs/.xvpics 

backup:
	cd ..;tar czf astrocompendium.tgz Astrocompendium/
