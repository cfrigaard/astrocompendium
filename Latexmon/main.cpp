#include <fstream>
#include <iostream>
#include <string>
#include <cstdio>
#include <windows.h>

using namespace std;
 
string getpath(string s)
{
  if (s.size()<1) return s; // odd path
  const char c=s[s.size()-1];

  if(c=='\\' || c=='/') s=s.substr(0,s.size()-1);

  size_t n1=s.find_last_of('\\');
  return s.substr(n1+1,s.size()) + "\\";

  /*
  size_t n2=s.find_last_of('/');
  size_t n3=s.find_last_of('&:');
  
  return s.substr((max(n1,n2),n3),s.size());
  */
}

int system(const string& cmd){return system(cmd.c_str());}
int del   (const string& file){
  ifstream is(file.c_str());
  if (is){
    is.close();
    system("del " + file); 
  }
  ifstream is2(file.c_str());
  if (is2){
      is.close();
      cerr << "\nCould not delete file <" << file << ">...aborting";
      return -1;
  }
  return 0;
}

int main(int argc,char** argv)
{
  if (argc!=3){
    cerr << "\nUsage: " << argv[0] << " <texfile> <directory>\n";
    return -1;
  }
  const string texfile=argv[1];
  const string texpath=argv[2];

  // xcopy only accepts win-backslash
  const string remoteroot="z:\\Temp\\RemoteLatex\\";
  const string remotepath=remoteroot + getpath(texpath); 
  const string remoteoutput=remotepath + "latexoutput.txt";

  cerr << "Remote latexing...\n   <" << texpath << texfile << ".tex> at\n   <" << remotepath << ">\n";
  
  // check correct directory
  {
    ifstream is((texfile + ".tex").c_str());
    if (!is){
      cerr << "\nCould not find <" << texfile << ".tex> file...correct directory?...aborting";
      return -1;
    }
  }

  /*
  system( "del latexoutput.txt" );  // del local output 
  system(("del " + remoteoutput).c_str() ); // del remote output
  {
    ifstream is(remoteoutput.c_str());
    if (is){
      cerr << "\nCould not delete file <" << remoteoutput << ">...aborting";
      return -1;
    }
  }
  */
  del("latexoutput.txt");
  del(remoteoutput);
  
  cerr << "Copying files...\n";
  system("xcopy /d /e /y /i /exclude:D:\\Astrocompendium\\Latexmon\\exclude_1.txt * " + remotepath);
  system("echo " + getpath(texpath) + " " + texfile + " >latex.go");
  system("copy latex.go " + remoteroot);
  cerr << "Waiting for reply...\n";
  
  int retry=0;
  while(retry++<100)
  { 
    Sleep(100);
    ifstream is(remoteoutput.c_str());
    if (is){
      is.close();
      cerr << "signal received...copying outupt files\n";
      system("copy " + remoteoutput + " .");
      const int n=system("copy " + remotepath + texfile + ".dvi.lzo .");
      if (n==0){
        system("cd " + remotepath + " & lzop -dfU " + texfile + ".dvi.lzo");
      }
      else system("copy " + remotepath + texfile +".dvi .");
   
      system("copy " + remotepath + texfile + ".log .");
      //system("copy " + remotepath + texfile + ".idx ."); 
      system("type latexoutput.txt");
      return 0; 
    }     
  }
   
  cerr << "No signal received...aborting";
  return -1;
}